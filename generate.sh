#!/bin/sh

cat << _EOF_
<!DOCTYPE html>
<html>
<head>
    <title>merge 🚂 deployments</title>
</head>

<body>
      <div>
              <h1><strong>What is this?</strong></h1>
              <p>It's a playground for merge-train based deployment.
              You can see the code on my <a href="https://gitlab.com/nolith/test-merge-train-deployments/">gitlab project</a>.</p>
      </div>
      <div>
        <h2>Deployment Merge Request data</h2>
        <ul>
                <li><em>Title</em> ${CI_MERGE_REQUEST_TITLE}</li>
                <li><em>IID</em> <a href="${CI_MERGE_REQUEST_PROJECT_URL}/-/merge_requests/${CI_MERGE_REQUEST_IID}">${CI_MERGE_REQUEST_IID}</a></li>
        </ul>
      <hr/>
      <div>
              Generated by pipeline <a href="${CI_PIPELINE_URL}">${CI_PIPELINE_ID}</a>
      </div>
</body>
</html>
_EOF_
