#!/bin/sh

set -e

echo "***** Deployment script *******"

mkdir -p public
mv index.html public/

# This is a dirty trick, I'm changing the deployment time based on the merge request title
# each MR MUST begin with "<NUMBER>:" this will extract the number and use it as a deployment time
variable_deploy_time=$(echo "${CI_MERGE_REQUEST_TITLE=10:}" | cut -d ":" -f 1)

echo "Deployment ETA: ${variable_deploy_time} seconds"

sleep $variable_deploy_time
